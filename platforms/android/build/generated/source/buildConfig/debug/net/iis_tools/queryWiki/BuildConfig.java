/**
 * Automatically generated file. DO NOT MODIFY
 */
package net.iis_tools.queryWiki;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "net.iis_tools.queryWiki";
  public static final String BUILD_TYPE = "debug";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 100000;
  public static final String VERSION_NAME = "";
}
