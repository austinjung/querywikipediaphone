angular.module('queryWiki.controllers', ['ionic', 'LocalStorageModule'])

.controller('RootCtrl', function($scope, $http, $log, $ionicPopup, $ionicSideMenuDelegate, $cordovaDialogs, $timeout) {
    $scope.toggleMenu = function() {
        $ionicSideMenuDelegate.toggleLeft();
    };

    $scope.search = function() {
        $scope.toggleMenu();
        $scope.$broadcast('search');
    };

    $scope.$on('addBookmarks', function(obj, new_bookmark) {
        $scope.$broadcast('refreshBookmarks', new_bookmark);
    });

    $scope.filterByTitle = function(toggle) {
        $log.debug('Change title filter');
        $cordovaDialogs.prompt('Input a word to filter article.', 'Search', ['Search','Cancel'], '')
            .then(function(result) {
                var input = result.input1;
                // no button = 0, 'OK' = 1, 'Cancel' = 2
                var btnIndex = result.buttonIndex;
                $log.debug('Filter by title : ' + input + ", " + btnIndex);
                // Default search filter = 'A'
                if (toggle) {
                    $scope.toggleMenu();
                }
                if (btnIndex == 2) {
                    input = 'A';
                }
                if (input == '') {
                    input = 'A';
                }
                $scope.$broadcast('filterByTitle', input);
        });
    };

    $scope.openNewWindow = function(url) {
        window.open(url, '_system', 'location=yes');
        return false;
    };

})

.controller('WikipediaArticlesCtrl', function($scope, $ionicLoading, $log, $http, $cordovaToast) {
    $scope.init = function(apiurl) {
        $scope.apiurl = apiurl;
        $scope.articles = null;
        $scope.promise = null;
        $scope.query = {
            title: 'A',
            format: 'json'
        };
        $scope.getData();
    };

    $scope.$on('filterByTitle', function(event, title) {
        if ($scope.apiurl) {
            $scope.query.title = title;
            $scope.getData();
        }
    });

    $scope.$on('search', function(event) {
        if ($scope.apiurl) {
            $scope.getData();
        }
    });

    $scope.getData = function() {
        if ($scope.promise || !$scope.apiurl) {
            return; // Cancel current http request if there is a pending http request
        }
        $ionicLoading.show({
            template: '<ion-spinner class="spinner-positive" icon="ios"></ion-spinner>'
        });
        $scope.promise = $http.get($scope.apiurl, {params: $scope.query})
            .success(function(data) {
                $scope.articles = data;
            })
            .error(function(data, status) {
                $ionicLoading.hide();
                $scope.promise = null;
                if (status !== 0 && data !== null) {
                    var msg = "Could not retrieve articles. Http error " + status + ".";
                    $log.error(msg);
                    $log.debug(data);
                    $cordovaToast.show(msg, 'long', 'bottom')
                                 .then(function(success){$log.debug('toast success: ' + success)}, function(error){alert('toast error: ' + error)});
                }
            })
            .then(function() {
                $ionicLoading.hide();
                $scope.promise = null;
            });
    };

    $scope.init('http://sharemedia.cloudapp.net/api/wiki/');

})

.controller('ArticleDetailCtrl', function($scope, $stateParams, $ionicLoading, $log, $http, $cordovaToast, localStorageService, $cordovaInAppBrowser) {
    $scope.init = function(apiurl) {
        $scope.apiurl = apiurl;
        $scope.article = null;
        $scope.promise = null;
        $scope.query = {
            title: $stateParams.articleTitle,
            format: 'json'
        };
        $scope.getData();
    };

    $scope.openWikiWindow = function() {
        window.open($scope.article.Url, '_system', 'location=yes');
        return false;
    };

    $scope.setBookmark = function() {
        var bookmarks = localStorageService.get('bookmarks');
        if (bookmarks === null) {
            bookmarks = [];
        }
        for (var index = 0; index < bookmarks.length; index++) {
            if (bookmarks[index].Text == $scope.article.Text) {
                return;
            }
        }
        bookmarks.push($scope.article);
        localStorageService.set('bookmarks', bookmarks);
        $scope.$emit('addBookmarks', $scope.article);
    };

    $scope.getData = function() {
        if ($scope.promise) {
            return; // Cancel current http request if there is a pending http request
        }
        $ionicLoading.show({
            template: '<ion-spinner class="spinner-positive" icon="ios"></ion-spinner>'
        });
        $scope.promise = $http.get($scope.apiurl, {params: $scope.query})
            .success(function(data) {
                $scope.article = data[0];
            })
            .error(function(data, status) {
                $ionicLoading.hide();
                $scope.promise = null;
                if (status !== 0 && data !== null) {
                    var msg = "Could not retrieve articles. Http error " + status + ".";
                    $log.error(msg);
                    $log.debug(data);
                    $cordovaToast.show(msg, 'long', 'bottom')
                                 .then(function(success){$log.debug('toast success: ' + success)}, function(error){alert('toast error: ' + error)});
                }
            })
            .then(function() {
                $ionicLoading.hide();
                $scope.promise = null;
            });
    };

    $scope.init('http://sharemedia.cloudapp.net/api/wiki/');

})

.controller('BookmarksCtrl', function($scope, localStorageService) {
    $scope.articles = localStorageService.get('bookmarks');

    $scope.$on('refreshBookmarks', function(obj, new_bookmark) {
        $scope.articles.push(new_bookmark);
    });

});
